civic_corvee_system = {
	description = "civic_corvee_system_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			NOR = {
				text = civic_tooltip_not_egalitarian_atlas
				value = ethic_egalitarian
				value = ethic_fanatic_egalitarian
				value = ethic_inventors
				value = ethic_fanatic_inventors
				value = ethic_hidden
				value = ethic_fanatic_hidden
			}
		}
		civics = {
			NOT = {
				value = civic_free_haven
			}
		}
	}
	random_weight = { base = 5 }
	modifier = {
		pop_growth_from_immigration = 0.15
	}
}

civic_imperial_cult = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		authority = {
			value = auth_imperial
		}
		ethics = {
			OR = {
				text = civic_tooltip_as_atlas
				value = ethic_godhood
				value = ethic_fanatic_godhood
				value = ethic_slave_keeper
				value = ethic_fanatic_slave_keeper
				value = ethic_holy_warrior
				value = ethic_fanatic_holy_warrior
				value = ethic_authoritarian
				value = ethic_fanatic_authoritarian
				value = ethic_spiritualist
				value = ethic_fanatic_spiritualist
			}
		}
	}
	random_weight = { base = 5 }
	modifier = {
		country_edict_cap_add = 2
	}
}

civic_beacon_of_liberty = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		authority = {
			value = auth_democratic
		}
		ethics = {
			OR = {
				text = civic_tooltip_te_atlas
				value = ethic_tech_masters
				value = ethic_fanatic_tech_masters
				value = ethic_egalitarian
				value = ethic_fanatic_egalitarian
			}
			NOR = {
				text = civic_tooltip_not_xenophobe_atlas
				value = ethic_xenophobe
				value = ethic_fanatic_xenophobe
				value = ethic_devourer
				value = ethic_fanatic_devourer
				value = ethic_hidden
				value = ethic_fanatic_hidden
			}
		}
	}
	random_weight = { base = 5 }
	modifier = {
		country_unity_produces_mult = 0.15
		empire_size_pops_mult = -0.15

	}
}

civic_exalted_priesthood = {
	description = "civic_tooltip_exalted_priesthood_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		authority = {
			OR = {
				value = auth_oligarchic
				value = auth_dictatorial
			}
		}
		ethics = {
			OR = {
				text = civic_tooltip_spiritualist_atlas
				value = ethic_spiritualist
				value = ethic_fanatic_spiritualist
				value = ethic_godhood
				value = ethic_fanatic_godhood
				value = ethic_holy_warrior
				value = ethic_fanatic_holy_warrior
			}
		}
		civics = {
			NOR = {
				value = civic_merchant_guilds
				value = civic_aristocratic_elite
				value = civic_technocracy
			}
		}
	}
	random_weight = { base = 5 }
}

civic_philosopher_king = {
	description = "civic_tooltip_philosopher_king_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		authority = {
			OR = {
				text = civic_tooltip_dic_imp
				value = auth_dictatorial
				value = auth_imperial
			}
		}
	}
	random_weight = { base = 5 }
	modifier = {
		ruler_skill_levels = 2
	}
}

civic_meritocracy = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		authority = {
			OR = {
				text = civic_tooltip_dem_oli
				value = auth_democratic
				value = auth_oligarchic
			}
		}
	}
	random_weight = { base = 5 }
	modifier = {
		leader_skill_levels = 1
		planet_jobs_specialist_produces_mult = 0.10
	}
}

civic_citizen_service = {
	description = "civic_tooltip_citizen_service_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				text = civic_tooltip_militarist_atlas
				value = ethic_militarist
				value = ethic_fanatic_militarist
				value = ethic_holy_warrior
				value = ethic_fanatic_holy_warrior
				value = ethic_devourer
				value = ethic_fanatic_devourer
			}
			NOR = {
				text = civic_tooltip_not_f_xenophile_atlas
				value = ethic_fanatic_xenophile
				value = ethic_fanatic_peace_keeper
				value = ethic_fanatic_slave_keeper
			}
		}
		authority = {
			OR = {
				text = civic_tooltip_dem_oli
				value = auth_democratic
				value = auth_oligarchic
			}
		}
		civics = {
			NOT = { value = civic_reanimated_armies }
		}
	}
	random_weight = { base = 5 }
	modifier = {
		country_naval_cap_mult = 0.15
	}
}

civic_technocracy = {
	description = civic_tooltip_technocracy_effects
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				text = civic_tooltip_materialist_atlas
				value = ethic_fanatic_inventors
				value = ethic_fanatic_materialist
				value = ethic_fanatic_tech_masters
			}
		}
		civics = {
			NOR = {
				value = civic_merchant_guilds
				value = civic_exalted_priesthood
				value = civic_aristocratic_elite
			}
		}
	}
	random_weight = { base = 5 }
}

civic_feudal_realm = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		authority = {
			value = auth_imperial
		}
	}
	random_weight = { base = 5 }

	description = "civic_tooltip_feudal_realm_effects"
	modifier = {
		country_subject_power_penalty_mult = -0.5
	}
}

civic_police_state = {
	description = "civic_tooltip_police_state_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	modifier = {
		planet_stability_add = 5
	}
	possible = {
		ethics = {
			NOR = { 
				text = civic_tooltip_not_f_egalitarian_atlas
				value = ethic_fanatic_egalitarian 
				value = ethic_fanatic_hidden
				value = ethic_fanatic_inventors
			}
		}
	}
	random_weight = { base = 5 }
}

civic_idealistic_foundation = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				text = civic_tooltip_egalitarian_atlas
				value = ethic_egalitarian
				value = ethic_fanatic_egalitarian
				value = ethic_hidden
				value = ethic_fanatic_hidden
				value = ethic_inventors
				value = ethic_fanatic_inventors
			}
		}
	}
	random_weight = { base = 5 }
	modifier = {
		pop_citizen_happiness = 0.10
	}
}

civic_environmentalist = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	random_weight = { base = 5 }
	modifier = {
		planet_pops_consumer_goods_upkeep_mult = -0.20
	}
}

civic_slaver_guilds = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				text = civic_tooltip_authoritarian_atlas
				value = ethic_authoritarian
				value = ethic_fanatic_authoritarian
				value = ethic_godhood
				value = ethic_fanatic_godhood
				value = ethic_slave_keeper
				value = ethic_fanatic_slave_keeper
			}
		}
		civics = { NOT = { value = civic_pleasure_seekers } }
	}
	random_weight = { base = 5 }
	modifier = {
		planet_jobs_slave_produces_mult = 0.10
		country_pop_enslaved_mult = 0.35
	}
}

civic_inwards_perfection = {
	modification = no
	description = "civic_tooltip_inward_perfection_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				text = civic_tooltip_pacifist_atlas
				value = ethic_pacifist
				value = ethic_fanatic_pacifist
				value = ethic_peace_keeper
				value = ethic_fanatic_peace_keeper
				value = ethic_tech_masters
				value = ethic_fanatic_tech_masters
			}
			OR = {
				text = civic_tooltip_xenophobe
				value = ethic_xenophobe
				value = ethic_fanatic_xenophobe
			}
		}
		origin = {
			NOR = {
				value = origin_common_ground
				value = origin_hegemon
			}
		}
		civics = {
			NOT = {
				value = civic_pompous_purists
			}
		}
	}
	random_weight = { base = 1000 }
	modifier = {
		country_unity_produces_mult = 0.20
		pop_growth_speed = 0.10
		country_edict_cap_add = 1
		pop_citizen_happiness = 0.05
		envoys_add = -1
		intel_encryption_add = 1
		intel_decryption_add = -1
	}
}

civic_warrior_culture = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				text = civic_tooltip_militarist_atlas
				value = ethic_militarist
				value = ethic_fanatic_militarist
				value = ethic_holy_warrior
				value = ethic_fanatic_holy_warrior
				value = ethic_devourer
				value = ethic_fanatic_devourer
			}
		}
		civics = {
			NOT = {
				value = civic_pleasure_seekers
			}
		}
	}
	random_weight = { base = 5 }
	description = "civic_tooltip_warrior_culture_effects"
	modifier = {
		army_damage_mult = 0.20
	}
}

civic_distinguished_admiralty = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				text = civic_tooltip_militarist_atlas
				value = ethic_militarist
				value = ethic_fanatic_militarist
				value = ethic_holy_warrior
				value = ethic_fanatic_holy_warrior
				value = ethic_devourer
				value = ethic_fanatic_devourer
			}
		}
	}
	random_weight = { base = 5 }
	modifier = {
		ship_fire_rate_mult = 0.10
		admiral_skill_levels = 1
		country_command_limit_add = 10
	}
}

civic_free_haven = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				text = civic_tooltip_xenophile_atlas
				value = ethic_xenophile
				value = ethic_fanatic_xenophile
				value = ethic_peace_keeper
				value = ethic_fanatic_peace_keeper
				value = ethic_slave_keeper
				value = ethic_fanatic_slave_keeper
			}
		}
		civics = {
			NOT = {
				value = civic_corvee_system
			}
		}
	}
	random_weight = { base = 5 }
	modifier = {
		pop_growth_from_immigration = 0.15
		planet_immigration_pull_mult = 0.50
	}
}

civic_cutthroat_politics = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	random_weight = { base = 5 }
	modifier = {
		edicts_cost_mult = -0.20
		intel_decryption_add = 1
	}
}

# Disable this civic if host has MegaCorp as it's replaced by the Corporate authority
civic_corporate_dominion = {
	playable = { NOT = { host_has_dlc = "Megacorp" } }
	ai_playable = { NOT = { host_has_dlc = "Megacorp" } }
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		authority = {
			value = auth_oligarchic
		}
		ethics = {
			NOR = {
				text = civic_tooltip_not_xenophobe_atlas
				value = ethic_xenophobe
				value = ethic_fanatic_xenophobe
				value = ethic_hidden
				value = ethic_fanatic_hidden
				value = ethic_devourer
				value = ethic_fanatic_devourer
			}
		}
	}
	random_weight = {
		base = 3
		modifier = {
			factor = 0
			host_has_dlc = "Megacorp"
		}
	}

	# unlocks sponsored_colonizer ship size

	description = "civic_tooltip_corporate_dominion_effects"
}

civic_agrarian_idyll = {
	description = "civic_tooltip_agrarian_idyll_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				text = civic_tooltip_pacifist_atlas
				value = ethic_pacifist
				value = ethic_fanatic_pacifist
				value = ethic_peace_keeper
				value = ethic_fanatic_peace_keeper
				value = ethic_tech_masters
				value = ethic_fanatic_tech_masters
			}
		}
		origin = {
			NOR = {
				value = origin_post_apocalyptic
				value = origin_shattered_ring
				value = origin_void_dwellers
				value = origin_remnants
			}
		}
		civics = { NOT = { value = civic_anglers } }
	}
	random_weight = { base = 5 }
	modifier = {}
}

civic_shadow_council = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		authority = {
			OR = {
				text = civic_tooltip_dem_oli_dic
				value = auth_democratic
				value = auth_oligarchic
				value = auth_dictatorial
			}
		}
	}
	random_weight = { base = 5 }
	modifier = {
		country_election_influence_cost_mult = -0.75
		planet_jobs_ruler_produces_mult = 0.10
		intel_decryption_add = 1
	}
}

civic_mining_guilds = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	random_weight = { base = 5 }
	modifier = {
		planet_miners_minerals_produces_add = 1
	}
}

civic_parliamentary_system = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		authority = {
			value = auth_democratic
		}
	}
	random_weight = { base = 5 }
	modifier = {
		pop_factions_produces_mult = 0.40
	}
}

civic_efficient_bureaucracy = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	random_weight = { base = 5 }
	modifier = {
		country_admin_cap_mult = 0.20
	}
}

civic_nationalistic_zeal = {
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				text = civic_tooltip_militarist_atlas
				value = ethic_militarist
				value = ethic_fanatic_militarist
				value = ethic_holy_warrior
				value = ethic_fanatic_holy_warrior
				value = ethic_devourer
				value = ethic_fanatic_devourer
			}
		}
	}
	random_weight = { base = 5 }
	modifier = {
		country_war_exhaustion_mult = -0.2
		country_claim_influence_cost_mult = -0.15
	}
}

civic_functional_architecture = {
	description = "civic_tooltip_functional_architecture_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	random_weight = { base = 5 }
	modifier = {
		planet_structures_cost_mult = -0.15
		planet_max_buildings_add = 1
	}
}

civic_aristocratic_elite = {
	description = "civic_tooltip_aristocratic_elite_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	modifier = {
		governor_skill_levels = 1
	}
	possible = {
		ethics = {
			NOR = {
				text = civic_tooltip_not_egalitarian_atlas
				value = ethic_egalitarian
				value = ethic_fanatic_egalitarian
				value = ethic_inventors
				value = ethic_fanatic_inventors
				value = ethic_hidden
				value = ethic_fanatic_hidden
			}
		}
		authority = {
			OR = {
				text = civic_tooltip_oli_imp
				value = auth_oligarchic
				value = auth_imperial
			}
		}
		civics = {
			NOR = {
				value = civic_merchant_guilds
				value = civic_exalted_priesthood
				value = civic_technocracy
			}
		}
	}
	random_weight = { base = 5 }
}

civic_shared_burden = {
	playable = { host_has_dlc = "Megacorp" }
	ai_playable = { host_has_dlc = "Megacorp" }
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				value = ethic_fanatic_egalitarian
				value = ethic_fanatic_inventors
			}
			NOR = {
				text = civic_tooltip_not_xenophobe_atlas
				value = ethic_xenophobe
				value = ethic_fanatic_xenophobe
				value = ethic_hidden
				value = ethic_fanatic_hidden
				value = ethic_devourer
				value = ethic_fanatic_devourer
			}
		}
		civics = {
			NOR = { 
				value = civic_technocracy 
				value = civic_pleasure_seekers
			}

		}
	}
	modifier = {
		planet_stability_add = 5
		pop_demotion_time_mult = -0.45
	}
	random_weight = {
		base = 1 # Should be fairly rare
	}

	description = civic_tooltip_shared_burden_effects
}

civic_fanatic_purifiers = {
	modification = no
	playable = { host_has_dlc = "Utopia" }
	ai_playable = { host_has_dlc = "Utopia" }
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				text = civic_tooltip_f_xenophobe_atlas
				value = ethic_fanatic_xenophobe
				value = ethic_fanatic_devourer
				value = ethic_fanatic_hidden
			}
			OR = {
				text = civic_tooltip_ms_atlas
				value = ethic_militarist
				value = ethic_spiritualist
				value = ethic_holy_warrior
				value = ethic_devourer
				value = ethic_godhood
			}
		}
		civics = {
			NOR = {
				value = civic_barbaric_despoilers
				value = civic_pompous_purists
			}
		}
		origin = {
			NOR = {
				value = origin_syncretic_evolution
				value = origin_slaver
				value = origin_godhood
				value = origin_common_ground
				value = origin_hegemon
			}
		}
	}
	random_weight = {
		base = 1000
		modifier = {
			factor = 0
			NOT = { host_has_dlc = "Utopia" }
		}
	}

	description = "civic_tooltip_fanatic_purifiers_effects"
	modifier = {
		ship_fire_rate_mult = 0.33
		army_damage_mult = 0.33
		starbase_shipyard_build_cost_mult = -0.15
		country_naval_cap_mult = 0.33
	}
}

civic_barbaric_despoilers = {
	playable = { host_has_dlc = "Apocalypse" }
	ai_playable = { host_has_dlc = "Apocalypse" }
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				text = civic_tooltip_militarist_atlas
				value = ethic_fanatic_militarist
				value = ethic_militarist
				value = ethic_fanatic_devourer
				value = ethic_devourer
				value = ethic_fanatic_holy_warrior
				value = ethic_holy_warrior
			}
			OR = {
				text = civic_tooltip_ax_atlas
				value = ethic_fanatic_authoritarian
				value = ethic_authoritarian
				value = ethic_fanatic_xenophobe
				value = ethic_xenophobe
				value = ethic_godhood
				value = ethic_fanatic_godhood
				value = ethic_fanatic_devourer
				value = ethic_devourer
			}
			NOR = {
				text = civic_tooltip_not_xenophile_atlas
				value = ethic_xenophile
				value = ethic_fanatic_xenophile
				value = ethic_peace_keeper
				value = ethic_fanatic_peace_keeper
				value = ethic_slave_keeper
				value = ethic_fanatic_slave_keeper
			}
		}
		civics = {
			NOT = {
				value = civic_fanatic_purifiers
			}
		}
		origin = {
			NOT = {
				value = origin_common_ground
			}
		}
	}
	modification = no
	random_weight = {
		base = 2
		modifier = {
			factor = 0
			NOT = { host_has_dlc = "Apocalypse" }
		}
		modifier = {
			# civic has no effect after galaxy generation, disable for random generation
			factor = 0
			has_global_flag = game_started
		}
	}
	description = "civic_tooltip_barbaric_despoilers_effects"
}

# New for MegaCorp
civic_byzantine_bureaucracy = {
	playable = { host_has_dlc = "Megacorp" }
	ai_playable = { host_has_dlc = "Megacorp" }
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		always = yes
	}
	random_weight = {
		base = 2
	}
	description = "civic_tooltip_byzantine_bureaucracy_effects"
}

civic_merchant_guilds = {
	description = "civic_tooltip_merchant_guilds_effects"
	playable = { host_has_dlc = "Megacorp" }
	ai_playable = { host_has_dlc = "Megacorp" }
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		civics = {
			NOR = {
				value = civic_exalted_priesthood
				value = civic_aristocratic_elite
				value = civic_technocracy
			}
		}
	}
	random_weight = {
		base = 2
	}
}

civic_reanimated_armies = {
	playable = { host_has_dlc = "Necroids Species Pack" }
	ai_playable = { host_has_dlc = "Necroids Species Pack" }
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			NOR = {
				text = civic_tooltip_not_gppt_atlas
				value = ethic_gestalt_consciousness
				value = ethic_pacifist
				value = ethic_fanatic_pacifist
				value = ethic_peace_keeper
				value = ethic_fanatic_peace_keeper
				value = ethic_tech_masters
				value = ethic_fanatic_tech_masters
			}
		}
		civics = {
			NOT = { value = civic_citizen_service }
		}
		species_archetype = {
			NOT = {
				text = disabled_by_synthetic_ascension
				value = ROBOT
			}
		}
	}
	random_weight = {
		base = 3
	}
	description = "civic_tooltip_reanimated_armies_effects"
}

civic_diplomatic_corps = {
	playable = { host_has_dlc = "Federations" }
	ai_playable = { host_has_dlc = "Federations" }
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		civics = {
			NOR = {
				value = civic_fanatic_purifiers
				value = civic_inwards_perfection
			}
		}
	}

	modifier = {
		envoys_add = 2
		diplo_weight_mult = 0.1
	}
}

civic_death_cult = {
	playable = { host_has_dlc = "Necroids Species Pack" }
	ai_playable = { host_has_dlc = "Necroids Species Pack" }
	description = "civic_tooltip_death_cult_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } } #Corps get their own version
	}
	possible = {
		origin = {
			NOT = { value = origin_necrophage }
		}
		civics = {
			NOR = {
				value = civic_fanatic_purifiers
				value = civic_inwards_perfection
				value = civic_ancient_preservers
			}
		}
		ethics = {
			OR = {
				text = civic_tooltip_spiritualist_atlas
				value = ethic_spiritualist
				value = ethic_fanatic_spiritualist
				value = ethic_holy_warrior
				value = ethic_fanatic_holy_warrior
				value = ethic_godhood
				value = ethic_fanatic_godhood
			}
		}
	}
}

civic_memorialist = {
	playable = { host_has_dlc = "Necroids Species Pack" }
	ai_playable = { host_has_dlc = "Necroids Species Pack" }
	description = "civic_tooltip_memorialist_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } } #Gestalts get their own versions
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		civics = {
			NOT = {
				value = civic_fanatic_purifiers
			}
		}
	}
}

civic_catalytic_processing = {
	playable = { host_has_dlc = "Plantoids Species Pack" }
	ai_playable = { host_has_dlc = "Plantoids Species Pack" }
	description = "civic_tooltip_catalytic_processing_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } } #Gestalts get their own versions
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		origin = {
			NOT = {
				value = origin_lithoid
			}
		}
		civics = {
			NOT = {
				value = civic_corporate_catalytic_processing
			}
		}
	}
	random_weight = { base = 5 }
}

civic_crafters = {
	playable = { host_has_dlc = "Humanoids Species Pack" }
	ai_playable = { host_has_dlc = "Humanoids Species Pack" }
	description = "civic_tooltip_crafters_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		always = yes
	}
	random_weight = {
		base = 3
	}
}

civic_pleasure_seekers = {
	playable = { host_has_dlc = "Humanoids Species Pack" }
	ai_playable = { host_has_dlc = "Humanoids Species Pack" }
	description = "civic_tooltip_pleasure_seekers_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		civics = {
			NOR = {
				value = civic_warrior_culture
				value = civic_shared_burden
				value = civic_slaver_guilds # would enslave hedonists
			}
		}
	}
	random_weight = {
		base = 3
	}
}

civic_idyllic_bloom = {
	modification = no
	playable = { host_has_dlc = "Plantoids Species Pack" }
	ai_playable = { host_has_dlc = "Plantoids Species Pack" }
	description = "civic_tooltip_idyllic_bloom_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		species_class = {
			OR = {
				value = FUN
				value = PLANT
			}
		}
		origin = {
			NOR = {
				text = civic_tooltip_not_origin_non_standard_habitability_preference
				value = origin_life_seeded
				value = origin_void_dwellers
				value = origin_shattered_ring
			}
		}
	}
}

civic_anglers = {
	modification = no
	playable = { has_aquatics = yes }
	ai_playable = { has_aquatics = yes }
	description = "civic_tooltip_anglers_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		origin = {
			NOR = {
				value = origin_post_apocalyptic
				value = origin_shattered_ring
				value = origin_void_dwellers
			}
		}
		civics = { NOT = { value = civic_agrarian_idyll } }
	}
	traits = {
		trait = trait_aquatic
	}
}

civic_pompous_purists = {
	playable = { has_humanoids = yes }
	ai_playable = { has_humanoids = yes }
	description = "civic_tooltip_pompous_purists_effects"
	potential = {
		ethics = { NOT = { value = ethic_gestalt_consciousness } }
		authority = { NOT = { value = auth_corporate } }
	}
	possible = {
		ethics = {
			OR = {
				text = civic_tooltip_xenophobe_atlas
				value = ethic_xenophobe
				value = ethic_fanatic_xenophobe
				value = ethic_devourer
				value = ethic_fanatic_devourer
				value = ethic_hidden
				value = ethic_fanatic_hidden
			}
		}
		civics = {
			NOR = {
				value = civic_fanatic_purifiers
				value = civic_inwards_perfection
			}
		}
		origin = { NOT = { value = origin_scion } }
	}

	modifier = {
		country_trust_growth = 0.30
		envoys_add = 2
	}
}

