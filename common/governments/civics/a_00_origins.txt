origin_slaver = {
	is_origin = yes
	icon = "gfx/interface/icons/origins/origins_syncretic_evolution.dds"
	picture = GFX_origin_syncretic_evolution

	potential = {
		always = yes
	}

	playable = {
		host_has_dlc = "Utopia"
	}

	possible = {
		ethics = {
			NOT = { 
				value = ethic_gestalt_consciousness 
			} 
			OR = {
				value = ethic_fanatic_slave_keeper
				value = ethic_slave_keeper
			}
		}
		civics = { NOT = { value = civic_fanatic_purifiers } }
	}

	description = civic_tooltip_syncretic_evolution_effects

	has_secondary_species = {
		title = civic_syncretic_evolution_secondary_species
		traits = {
			trait = trait_syncretic_proles
			trait = trait_slaver
		}
	}

	advanced_start = yes

	random_weight = {
		base = 100
	}
}

origin_godhood = {
	is_origin = yes
	icon = "gfx/interface/icons/origins/origins_syncretic_evolution.dds"
	picture = GFX_origin_syncretic_evolution

	potential = {
		always = yes
	}

	playable = {
		host_has_dlc = "Utopia"
	}

	possible = {
		ethics = { 
			NOT = { 
				value = ethic_gestalt_consciousness 
			} 
			OR = {
				value = ethic_fanatic_godhood
				value = ethic_godhood
			}
		}
		civics = { NOT = { value = civic_fanatic_purifiers } }
	}

	description = civic_tooltip_syncretic_evolution_effects

	has_secondary_species = {
		title = civic_syncretic_evolution_secondary_species
		traits = {
			trait = trait_syncretic_proles
			trait = trait_godhood_2
			trait = trait_docile
		}
	}

	advanced_start = yes

	random_weight = {
		base = 100
	}
}